<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    //
    
    
    protected $guarded=[];
    public function jobRequests(){
        return $this->hasMany(JobRequest::class);
    }
}
