<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Service extends Model
{
    public $name;
    public $info;
    public $images;
    public $videos;
    public $created_at;
    public $updated_at;
    protected $fillable = ['name','info','images','videos'];

    public function getImagesAttribute($value)
    {
        return json_decode($value);
    }
    public function setImagesAttribute($value)
    {
        $this->attributes['images'] = json_encode($value);
    }
    public function getVideosAttribute($value)
    {
        return json_decode($value);
    }
    public function setVideosAttribute($value)
    {
        $this->attributes['videos'] = json_encode($value);
    }
}
