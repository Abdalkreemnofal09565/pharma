<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{

    protected $fillable = ['name', 'info', 'price', 'images', 'videos', 'expiry', 'company', 'type_id', 'classification_id'];
    //protected $guarded = [];
    protected $appends = ['type_name', 'classification_name'];

    public function alternative()
    {
        return $this->hasMany(Product::class);
    }

    public function getTypeNameAttribute()
    {
        return $this->type ? $this->type->name : null;
    }

    public function getClassificationNameAttribute()
    {
        return $this->classification ? $this->classification->name : null;
    }

    public function classification()
    {
        return $this->belongsTo(Classification::class);
    }

    public function type()
    {
        return $this->belongsTo(Type::class);
    }

    public function getImagesAttribute($value)
    {
        return json_decode($value);
    }

    public function setImagesAttribute($value)
    {

        $this->attributes['images'] = json_encode($value);
    }

    public function getVideosAttribute($value)
    {
        return json_decode($value);
    }

    public function setVideosAttribute($value)
    {
        $this->attributes['videos'] = json_encode($value);
    }

}
