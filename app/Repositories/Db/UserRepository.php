<?php


namespace App\Repositories\Db;


use App\Models\Customer;
use App\Models\User;
use App\Repositories\IRepositories\IUserRepository;

class UserRepository extends BaseRepository implements IUserRepository
{
    /**
     * Specify Model class name
     *
     * @return mixed
     */
    public function model()
    {
        return User::class;
    }
}