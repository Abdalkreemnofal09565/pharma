<?php


namespace App\Repositories\Eloquent;

use App\Models\Customer;
use App\Repositories\IRepositories\ICustomerRepository;

 class CustomerRepository extends BaseRepository implements ICustomerRepository
{

    /**
     * Specify Model class name
     *
     * @return mixed
     */
    public function model()
    {
        return Customer::class;
    }
}