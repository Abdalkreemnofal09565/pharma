<?php


namespace App\Repositories\Eloquent;


use App\Models\Service;
use App\Repositories\IRepositories\IServiceRepository;

class ServiceRepository extends BaseRepository implements IServiceRepository
{
    public function model()
    {
       return Service::class;
    }
}