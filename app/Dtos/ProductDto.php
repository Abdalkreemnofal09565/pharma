<?php


namespace App\Dtos;


class ProductDto extends Dto implements \JsonSerializable
{
    protected $id;
    protected $name;
    protected $company;
    protected $price;
    protected $typeId;
    protected $typeName;
    protected $classificationId;
    protected $classificationName;
    protected $images;
    protected $videos;
    protected $expiry;
    protected $info;
    protected $createdAt;
    protected $updatedAt;

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }
    /**
     * @return mixed
     */
    public function getTypeId()
    {
        return $this->typeId;
    }
    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getInfo()
    {
        return $this->info;
    }

    /**
     * @param mixed $info
     */
    public function setInfo($info): void
    {
        $this->info = $info;
    }

    /**
     * @return mixed
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * @param mixed $images
     */
    public function setImages($images): void
    {
        $this->images = $images;
    }

    /**
     * @return mixed
     */
    public function getClassificationId()
    {
        return $this->classificationId;
    }

    /**
     * @param mixed $classificationId
     */
    public function setClassificationId($classificationId): void
    {
        $this->classificationId = $classificationId;
    }

    /**
     * @return mixed
     */
    public function getClassificationName()
    {
        return $this->classificationName;
    }

    /**
     * @param mixed $classificationName
     */
    public function setClassificationName($classificationName): void
    {
        $this->classificationName = $classificationName;
    }

    /**
     * @param mixed $typeId
     */
    public function setTypeId($typeId): void
    {
        $this->typeId = $typeId;
    }


    /**
     * @return mixed
     */
    public function getVideos()
    {
        return $this->videos;
    }

    /**
     * @param mixed $videos
     */
    public function setVideos($videos): void
    {
        $this->videos = $videos;
    }

    /**
     * @return mixed
     */
    public function getCompany()
    {
        return $this->company;
    }

    /**
     * @return mixed
     */
    public function getTypeName()
    {
        return $this->typeName;
    }
    /**
     * @param mixed $typeName
     */
    public function setTypeName($typeName): void
    {
        $this->typeName = $typeName;
    }
    /**
     * @param mixed $company
     */
    public function setCompany($company): void
    {
        $this->company = $company;
    }

    /**
     * @return mixed
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price): void
    {
        $this->price = $price;
    }

    /**
     * @return mixed
     */
    public function getExpiry()
    {
        return $this->expiry;
    }

    /**
     * @param mixed $expiry
     */
    public function setExpiry($expiry): void
    {
        $this->expiry = $expiry;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * @return mixed
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @return mixed
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param mixed $updatedAt
     */
    public function setUpdatedAt($updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @param mixed $createdAt
     */
    public function setCreatedAt($createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @param mixed $id
     */
    public function setId($id): void
    {
        $this->id = $id;
    }
    public static function rules() {
        return [

            "name" => "required",
            "typeId" => "required",
            "classificationId" => "required",
            "info" => "required",
            //"images" => "required",
            //"videos" => "required",
            "expiry" => "required",
            "price" => "required",
            "company" => "required",
        ];
    }
}