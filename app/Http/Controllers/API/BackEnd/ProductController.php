<?php

namespace App\Http\Controllers\Api\BackEnd;

use App\Dtos\CustomerDto;
use App\Dtos\ProductDto;
use App\Helpers\JsonResponse;
use App\Helpers\Mapper;
use App\Helpers\ValidatorHelper;
use App\Http\Controllers\Controller;
use App\Models\Customer;
use App\Models\Product;
use App\Repositories\Eloquent\ServiceRepository;
use App\Repositories\IRepositories\ICustomerRepository;
use App\Repositories\IRepositories\IProductRepository;
use App\Repositories\IRepositories\IServiceRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ProductController extends Controller
{
    private $productRepository;
    private $serviceRepository;

    public function __construct(IProductRepository $productRepository/*, IServiceRepository $serviceRepository*/)
    {
        $this->productRepository = $productRepository;
        //$this->serviceRepository = $serviceRepository;
    }

    public function index()
    {
        $products = $this->productRepository->all();
        $productsDtos = Mapper::modelsToDtoArray($products, ProductDto::class);
        return JsonResponse::respondSuccess(JsonResponse::MSG_SUCCESS, $productsDtos);
    }
    public function store(Request $request)
    {
         $validator = Validator::make($request->all(), ProductDto::rules(),ValidatorHelper::messages());
        if ($validator->passes()) {
        $model = $this->productRepository->create($request->all());
        return JsonResponse::respondSuccess(JsonResponse::MSG_ADDED_SUCCESSFULLY);
          }
        return JsonResponse::respondError($validator->errors()->all());
    }

    public function destroy(Request $request)
    {
        $this->productRepository->delete(26);
        return JsonResponse::respondSuccess(JsonResponse::MSG_DELETED_SUCCESSFULLY);
    }
    public function update(Request $request,$id)
    {
        $validator = Validator::make($request->all(), ProductDto::rules(),ValidatorHelper::messages());
        if ($validator->passes()) {
            $rt = $this->productRepository->update($request->all(),$id);
            return JsonResponse::respondSuccess(JsonResponse::MSG_UPDATED_SUCCESSFULLY);

        }
        return JsonResponse::respondError($validator->errors()->all());

    }

    public function getUser()
    {
        return [
            "name" => "bbt",
            "company" => "hjhg",
            "price" => "722",
            "typeId" => "5",
            "classificationId" => 4,
            "images" => "sdsdfsdfsdfsf sdfsd fdsfs sdfd s",
            "videos" => "sdfsd sfsdlfm sdfds,fms",
            "expiry" => "2017-02-12 11:07:26",
            "info" => "asfdsg dgdgf fdgfd gfdgfdg"];
    }

}
