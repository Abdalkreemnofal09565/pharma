<?php

namespace App\Http\Controllers\API\BackEnd;

use App\Dtos\ProductDto;
use App\Dtos\ServiceDto;
use App\Helpers\JsonResponse;
use App\Helpers\Mapper;
use App\Helpers\ValidatorHelper;
use App\Http\Controllers\Controller;
use App\Models\Service;
use App\Repositories\IRepositories\IProductRepository;
use App\Repositories\IRepositories\IServiceRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class ServiceController extends Controller
{
   private $serviceRepository ;
   //private $productRepository ;
   public function __construct(IServiceRepository $serviceRepository,IProductRepository $productRepository)
   {
       $this->serviceRepository = $serviceRepository;
      // $this->productRepository = $productRepository;
   }

    public function index()
    {
        $services = $this->serviceRepository->all();
        $servicesDtos = Mapper::modelsToDtoArray($services, ServiceDto::class);
        return JsonResponse::respondSuccess(JsonResponse::MSG_SUCCESS, $servicesDtos);
    }
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), ServiceDto::rules(),ValidatorHelper::messages());
        if ($validator->passes()) {
            $model = $this->serviceRepository->create($request->all());
            return JsonResponse::respondSuccess(JsonResponse::MSG_ADDED_SUCCESSFULLY);
        }
        return JsonResponse::respondError($validator->errors()->all());
    }

    public function destroy(Request $request)
    {
        $this->serviceRepository->delete(26);
        return JsonResponse::respondSuccess(JsonResponse::MSG_DELETED_SUCCESSFULLY);
    }
    public function update(Request $request,$id)
    {
        $validator = Validator::make($request->all(), ServiceDto::rules(),ValidatorHelper::messages());
        if ($validator->passes()) {
            $rt = $this->serviceRepository->update($request->all(),$id);
            return JsonResponse::respondSuccess(JsonResponse::MSG_UPDATED_SUCCESSFULLY);

        }
        return JsonResponse::respondError($validator->errors()->all());

    }

}
