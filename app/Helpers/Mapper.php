<?php


namespace App\Helpers;


use App\Dtos\CustomerDto;
use App\Dtos\Dto;
use AutoMapperPlus\AutoMapper;
use AutoMapperPlus\Configuration\AutoMapperConfig;
use AutoMapperPlus\Exception\UnregisteredMappingException;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class Mapper
{
    /**
     * @param $source
     * @param $dist
     * @return mixed|null
     */
    public static function convertObject($source, $dist)
    {
        try {
            $mappedRecord = config('DtoMapper')->map($source, $dist);
            return $mappedRecord;
        } catch (UnregisteredMappingException $e) {
            return $e->getMessage();
        }

    }

    /**
     * @param $source
     * @param $dist
     * @return mixed|null
     */
    public static function convertMultipleObjects2($source, $dist)
    {

        try {
            $mappedRecord = config('DtoMapper')->mapMultiple($source, $dist);
            return $mappedRecord;
        } catch (UnregisteredMappingException $e) {
            return $e->getMessage();
        }
    }

    public static function snakeToCamel($key)
    {
        // replace underscores with spaces, uppercase first letter of all words,
        // join them, lowercase the very first letter of the name
        return lcfirst(str_replace(' ', '', ucwords(str_replace('_', ' ', $key))));
    }

    public static function modelsToDtoArray($models, $dtoClass)
    {
        $dtos = array();
        foreach ($models as $model) {
            $dto = new $dtoClass();
            $attributes = $model->attributesToArray();
            foreach ($attributes as $key => $value) {
                $set_method_name = "set" . self::snakeToCamel($key);
                if (method_exists($dtoClass, $set_method_name)) {
                    $dto->{$set_method_name} ($value);
                }
            }
            array_push($dtos, $dto);
        }
        return $dtos;
    }

    public static function modelToDto($model, $dtoClass)
    {

        $dto = new $dtoClass();
        $attributes = $model->attributesToArray();
        foreach ($attributes as $key => $value) {
            $set_method_name = "set" . self::snakeToCamel($key);
            if (method_exists($dtoClass, $set_method_name)) {
                $dto->{$set_method_name} ($value);
            }
        }


        return $dto;
    }

    public static function ArrayToDto(array $request, Dto $dto)
    {


        foreach ($request as $key_name => $value) {
            $method_name = "set" . ucfirst($key_name);
            if (method_exists($dto, $method_name)) {
                $dto->{$method_name}($value);
            }
        }
        return $dto;
    }

    public static function DtoToArray(Dto $dto)
    {


        $array = json_decode(json_encode($dto), true);
        return $array;
    }

    /**
     * @param $model
     * @return mixed
     * @description map between model attributes and model variables
     */
    public static function setModelProperties($model)
    {

        $attributes = $model->getAttributes();
        foreach (get_object_vars($model) as $key_name => $value) {
            $method_name = "set" . ucfirst($key_name);
            if (method_exists($model, $method_name)) {
                $model->{$method_name}($value);
            } else {
                if (isset($attributes[$key_name]))
                    $model->{$key_name} = $attributes[$key_name];
            }
        }
        return $model;
    }

    public static function camelToUnderscore($string, $us = "-")
    {
        return strtolower(preg_replace(
            '/(?<=\d)(?=[A-Za-z])|(?<=[A-Za-z])(?=\d)|(?<=[a-z])(?=[A-Z])/', $us, $string));
    }
}