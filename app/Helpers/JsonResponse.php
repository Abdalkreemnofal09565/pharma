<?php


namespace App\Helpers;


class JsonResponse
{
    public const MSG_ADDED_SUCCESSFULLY = "record has been added successfully";
    public const MSG_UPDATED_SUCCESSFULLY = "record has been updated successfully";
    public const MSG_DELETED_SUCCESSFULLY = "record has been deleted successfully";
    public const MSG_NOT_ALLOWED = "action not allowed ";
    public const MSG_NOT_FOUND = "resource not found";
    public const MSG_SUCCESS= "Success";
    /**
     * @param null $content
     * @return \Illuminate\Http\JsonResponse
     */
    public static function respondSuccess($message ,$content = null, $status = 200){
        return response()->json([
            'result' => 'success',
            'data' => $content,
            'message' => $message,
            'status' => $status
        ]);
    }

    /**
     * @param $message
     * @return \Illuminate\Http\JsonResponse
     */
    public static function respondError($message,$status = 500){
        return response()->json([
            'result' => 'failed',
            'data' => null,
            'message' => $message,
            'status' => $status
        ]);
    }

}