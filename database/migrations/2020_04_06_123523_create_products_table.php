 <?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');

            $table->string('name');
            $table->unsignedBigInteger('type_id');
            $table->foreign('type_id')->references('id')->on('types')->onDelete("cascade")->onUpdate('cascade');
            $table->unsignedBigInteger('classification_id');
            $table->foreign('classification_id')->references('id')->on('classifications')->onDelete("cascade")->onUpdate('cascade');
            $table->string('company');
            $table->float('price');
            $table->dateTime('expiry');
            $table->longText('info');
             $table->unsignedBigInteger('alternative_id')->nullable(true);
            $table->foreign('alternative_id')->references('id')->on('products')->onDelete("cascade")->onUpdate('cascade');
            $table->text('images');
            $table->text('videos');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
