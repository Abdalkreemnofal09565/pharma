<?php

use Illuminate\Database\Seeder;

class TypesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Models\Type::class, 3)->create();
//        factory(\App\Models\Type::class, 3)->create()->each(function ($type) {
//            $type->products()->createMany(
//                factory(\App\Models\Product::class, 5)->make()->toArray()
//            );
//        });
    }
}
