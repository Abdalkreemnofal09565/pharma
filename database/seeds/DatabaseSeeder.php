<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(ClassificationsTableSeeder::class);
        $this->call(ServicesTableSeeder::class);
        $this->call(TypesTableSeeder::class);
        $this->call(ProductsTableSeeder::class);
    }
}
