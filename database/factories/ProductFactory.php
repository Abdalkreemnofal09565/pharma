<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Product;
use Faker\Generator as Faker;

$factory->define(Product::class, function (Faker $faker) {
    $images_array = array();
    for ($i=0 ; $i<10; $i++)
    {
        $images_array[$i]= $faker->imageUrl(640,480,null,true,"Product");
    }
    return [
        'name' => $faker->word,
        'company' => $faker->word,
        'info' => $faker->text,
        'images' => $images_array,
        'videos' => $images_array,
        'expiry' => $faker->dateTime,
        'price' => $faker->randomNumber(4),
        'type_id' => factory(\App\Models\Type::class),
        'classification_id' => factory(\App\Models\Classification::class),
        //'alternative_id' => factory(\App\Models\Product::class),
        'created_at' => $faker->dateTime,
        'updated_at' => $faker->dateTime
    ];
});
