<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Classification;
use Faker\Generator as Faker;

$factory->define(Classification::class, function (Faker $faker) {
    return [
        'name' => $faker->word(),
        'created_at' => $faker->time(),
        'updated_at' => $faker->time()
    ];
});
