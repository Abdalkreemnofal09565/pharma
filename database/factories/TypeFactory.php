<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */


use Faker\Generator as Faker;

$factory->define(\App\Models\Type::class, function (Faker $faker) {
    return [
        'name' => $faker->word,
        'created_at' => $faker->time(),
        'updated_at' => $faker->time()
    ];
});
