<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Service;
use Faker\Generator as Faker;

$factory->define(Service::class, function (Faker $faker) {
    $images_array = array();
    for ($i=0 ; $i<10; $i++)
    {
        $images_array[$i]= $faker->imageUrl(640,480,null,true,"Product");
    }
    return [
        'name' => $faker->word,
        'info' => $faker->text,
        'images' => $images_array,
        'videos' => $images_array,
        'created_at' => $faker->time(),
        'updated_at' => $faker->time()

    ];
});
