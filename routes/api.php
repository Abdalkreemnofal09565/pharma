<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Helpers\JsonResponse;
use App\Helpers\ResponseStatus ;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
Route::group(["namespace"=>"API\BackEnd"],function() {
    //services [index, show,  store, update, destroy]
    Route::apiResource('services', 'ServiceController');
    // products [index, show,  store, update, destroy]
    Route::apiResource('products', 'ProductController');

});

