<?php

use AutoMapperPlus\Configuration\AutoMapperConfig;
use AutoMapperPlus\AutoMapper;
use App\Models\Product;
use App\Dtos\ProductDto;
use AutoMapperPlus\NameConverter\NamingConvention\SnakeCaseNamingConvention;
use AutoMapperPlus\NameConverter\NamingConvention\CamelCaseNamingConvention;
$config = new AutoMapperConfig();
// map Product model to ProductDto
mapFields($config, Product::class, ProductDto::class,['id','name', 'info', 'price', 'images', 'videos', 'expiry', 'company', 'type_name','type_id', 'classification_id','classification_name','created_at','updated_at']);

//**********************pharma************************

function mapFields(AutoMapperConfig &$config, $source, $dist, $fields)
{
    $tmpConfig = $config->registerMapping($source, $dist)->withNamingConventions(
        new SnakeCaseNamingConvention (), // The naming convention of the source class.
        new CamelCaseNamingConvention()); // The naming convention of the destination class.;

    foreach ($fields as $key => $field) {
        if (is_numeric($key)) {
            $tempProp = $field;
        } else {
            $tempProp = $key;
        }
        $words = explode('_', $tempProp);

        $disKey = '';
        foreach ($words as $n => $value) {
            if ($n != 0) {
                $disKey .= ucfirst($value);
            } else {
                $disKey .= $value;
            }
        }
        $tmpConfig->forMember($disKey, function ($source) use ($tempProp) {
            return $source->{$tempProp} ;
        });
    }

    $tmpConfig->reverseMap();
}
$mapper = new AutoMapper($config);
return $mapper;

